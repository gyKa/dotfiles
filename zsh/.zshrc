if [ -r ~/.zsh_aliases ]; then
    source ~/.zsh_aliases
fi

# Load Oh My Zsh framework.
if [ -r ~/.zshrc-oh-my-zsh ]; then
    source ~/.zshrc-oh-my-zsh
fi

source "${HOME}/.zshrc_local" 2> /dev/null
