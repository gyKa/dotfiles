" Do not emulate Vi as closely as possible.
set nocompatible

" Enable file type detection.
filetype on

" Enable loading of plugin files for specific file types.
filetype plugin on

" Enable syntax highlighting.
syntax on

source $HOME/.vim/plugins.vim
