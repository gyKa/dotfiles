# dotfiles

Personal configuration files that are managed by [GNU Stow](https://www.gnu.org/software/stow/).

## Requirements

- git
- brew (for macOS only)

## Installation

Run the following commands in your terminal:

```sh
git clone https://gitlab.com/gyKa/dotfiles.git ~/.dotfiles
cd ~/.dotfiles
# on Linux run `xargs -a pkglist-base.txt sudo apt install`
# on Linux desktop, additionally run `xargs -a pkglist-desktop.txt sudo apt install`
# on Linux (WSL), additionally run `xargs -a pkglist-wsl.txt sudo apt install`

# base configuration for every macOS computer
rm ~/.zprofile  # required because of brew
brew bundle

# base configuration for every server (except Ubuntu server)
rm ~/.bashrc
stow -v bash

# base configuration for every desktop/wsl/laptop
stow -v bin git vim zsh
chsh -s $(which zsh)

# install oh-my-zsh
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" "" --keep-zshrc

# macbookpro specific configuration
brew bundle --file=~/.dotfiles/macbookpro.brewfile
stow -v zsh-macbookpro

# macbook air m2 specific configuration
stow -v ssh zsh-macbook-air-m2

# macmini m2 specific configuration
brew bundle --file=~/.dotfiles/macmini.brewfile
stow -v ssh zsh-macmini oh-my-zsh-macmini-m2

# wsl specific configuration
stow -v zsh-wsl

# work specific configuration
stow -v git-oxylabs mycli-oxylabs zsh-oxylabs
```

After installing, open a new terminal window to see effects.

## Update

```sh
cd ~/.dotfiles
git pull

# for every macOS computer
brew bundle --file=~/.dotfiles/Brewfile

# for macmini only
brew bundle --file=~/.dotfiles/macmini.brewfile

# for macbookpro m1 only
brew bundle --file=~/.dotfiles/macbookpro.brewfile

# for macbook air m2 only
brew bundle --file=~/.dotfiles/macbook-air-m2.brewfile

stow -v -R <your packages>
```
